import React from 'react';
import PostsScreen from './screens/PostsScreen'

class App extends React.Component {
  render() {
      return (
          <PostsScreen />
      )
  }
}

export default App;
