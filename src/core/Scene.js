import Director from './Director'

/**
 * @desc Сцена. Представляет всю бизнес-логику приложения. Содержит в себе актеров, декорации и директора.
 * @constructor
 * @param {string} name имя сцены
 */
let Scene = function () {
    this.actors = []
    this.decors = []
    this.director = new Director(this)
}

Scene.prototype.update = function () {}

/**
 * @desc Получить режиссера сцены
 * @returns {Director} director режиссер сцены
 */
Scene.prototype.getDirector = function () {
    return this.director
}

/**
 * @desc Добавить актера на сцену
 * @param {Actor} actor актер
 */
Scene.prototype.addActor = function (actor) {
    this.actors.push(actor)
}

/**
 * @desc Получить актера сцены
 * @param {string} name имя актера
 * @returns {Actor} actor актер
 */
Scene.prototype.getActor = function (name) {
    let actorIndex = this.actors.findIndex((actor, index) => {
        return actor.name === name
    })

    return this.actors[actorIndex]
}

/**
 * @desc Убрать актера со сцены
 * @param {string} name имя актера
 */
Scene.prototype.removeActor = function (name) {
    this.actors = this.actors.filter((actor, index) => {
        return actor.name !== name
    })
}

/**
 * @desc Получить всех актеров сцены
 * @returns {array} actors актеры
 */
Scene.prototype.getActors = function () {
    return this.actors
}

/**
 * @desc Добавить декорацию на сцену
 * @param {Decor} decor декорация
 */
Scene.prototype.addDecor = function (decor) {
    this.decors.push(decor)
}

/**
 * @desc Получить декорацию сцены
 * @param {string} name имя декорации
 * @returns {Decor} decor декорация
 */
Scene.prototype.getDecor = function (name) {
    let decorIndex = this.decors.findIndex((decor, index) => {
        return decor.name === name
    })

    return this.actors[decorIndex]
}

/**
 * @desc Убрать декорацию со сцены
 * @param {string} name имя декорации
 */
Scene.prototype.removeDecor = function (name) {
    this.decors = this.decors.filter((decor, index) => {
        return decor.name !== name
    })
}

Scene.instance = new Scene()

/**
 * @desc Получить экземпляр класса сцены
 * @returns {Scene} instance экземпляр класса
 */
Scene.getInstance = function () {
    return Scene.instance
}

export default Scene