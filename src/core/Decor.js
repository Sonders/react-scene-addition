/**
 * @desc Декорация. Не имеет поведения. Используется как хранилище статичных данных.
 * @constructor
 */
let Decor = function () {
    this.state = new Object()
}

export default Decor