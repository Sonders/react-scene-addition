import UpdateScreenCommand from '../commands/UpdateScreenCommand'

/**
 * @desc Директор. Выполняет роль управления актерами. Раздает команды.
 * @constructor
 * @param {Scene} scene сцена
 */
let Director = function (scene) {
    this.scene = scene
    this.updateScreenCommand = new UpdateScreenCommand(scene.getActors())
}

/**
 * @desc Обновление сцены
 * @param {Object} instructions инструкции для актеров
 */
Director.prototype.updateScene = function () {
    this.updateScreenCommand.execute({ updateAction: this.scene.update })
}

export default Director