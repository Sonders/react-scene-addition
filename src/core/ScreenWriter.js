import Scenario from './Scenario'

/**
 * @desc Сценарист. Занимается написанием сценария для актеров. Использует для своих идей внешние источники.
 * @constructor
 */
let ScreenWriter = function() {
    this.scenario = null
    this.sources = []
}

/**
 * @desc Добавить источник идей
 * @param {string} name название истоника
 * @param {function} source источник
 * @param {object} params параметры
 */
ScreenWriter.prototype.addSource = function (name, source, params) {
    this.sources.push({ name: name, source: async after => {
        await new Promise((resolve, reject) => {
            resolve(source(params, after))
        })
    }})
}

/**
 * @desc Создать сценарий
 * @param {string} sourceName название истоника
 * @returns {Scenario} scenario сценарий
 */
ScreenWriter.prototype.createScenario = async function (sourceName) {
    let createScenario = this.sources.find(source => {
        return source.name === sourceName
    }).source

    await new Promise((resolve, reject) => {
        resolve(createScenario(story => {
            this.scenario = new Scenario(sourceName, story)
        }))
    })

    return this.scenario
}

/**
 * @desc Получить сценарий
 * @returns {Scenario} scenario сценарий
 */
ScreenWriter.prototype.getScenario = function () {
    return this.scenario
}

export default ScreenWriter