import axios from 'axios'

/**
 * @desc Сервис предоставления данных с апи
 * @constructor
 */
let ApiService = function () {}

/**
 * @desc Пример использования. Получить список постов
 * @param {string} query строка запроса (uri)
 * @param {function} after callback-функция для передачи полученных данных сценаристу
 */
ApiService.prototype.getPosts = async function(query, after) {
    await axios.get('https://jsonplaceholder.typicode.com/posts' + objectToQuery(query)).then(response => {
        after(response.data)
    }).catch(error => {
        console.log(error)
    })
}

/**
 * @desc Пример использования. Получить список пользователей
 * @param {string} query строка запроса (uri)
 * @param {function} after callback-функция для передачи полученных данных сценаристу
 */
ApiService.prototype.getUsers = async function(query, after) {
    await axios.get('https://jsonplaceholder.typicode.com/users' + objectToQuery(query)).then(response => {
        after(response.data)
    }).catch(error => {
        console.log(error)
    })
}

ApiService.instance = new ApiService()

/**
 * @desc Получить экземпляр класса сервиса
 * @returns {ApiService} instance экземпляр класса
 */
ApiService.getInstance = function () {
    return ApiService.instance
}

/**
 * @desc Приводит объект к строке запроса
 * @param {object} object uri с параметрами
 * @returns {string} query строка запроса
 */
function objectToQuery(object) {
    let query = '?' + Object.keys(object).map(key => {
        return encodeURIComponent(key) + '=' + object[key]
    }).join('&')

    return query
}

export default ApiService