import React from 'react';
import Actor from '../core/Actor'
import ScreenWriter from '../core/ScreenWriter'
import ApiService from '../services/ApiService'
import ReactScene from '../scenes/ReactScene'

class PostsScreen extends React.Component {
    constructor(props) {
        super(props)

        let scene = ReactScene.getInstance()
        scene.addActor(new Actor('posts'))
        scene.addScreen('PostsScreen', this)

        let screenWriter = new ScreenWriter()
        screenWriter.addSource('posts', ApiService.getInstance().getPosts, {})

        this.scene = scene
        this.screenWriter = screenWriter
    }

    componentDidMount() {
        this.screenWriter.createScenario('posts').then(scenario => {
            this.scene.getActor('posts').setScenario(scenario)
        })
    }

    update() {
        this.forceUpdate()
    }

    render() {
        return (
            <Posts />
        )
    }
}

class Posts extends React.Component {
    constructor(props) {
        super(props)

        this.scene = ReactScene.getInstance()
    }

    render() {
       return (
           <div className="posts">
                {
                    this.scene.getActor('posts').getScenario().getStory().map((post, index) => {
                        return <div className="post" key={ index }>
                            <h2 className="title">{ post.title }</h2>
                            <div className="content">{ post.body }</div>
                        </div>
                    })
                }
            </div>
       )
    }
}

export default PostsScreen