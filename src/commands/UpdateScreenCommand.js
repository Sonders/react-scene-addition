let UpdateScreenCommand = function (actors) {
    this.actors = actors
}

UpdateScreenCommand.prototype.execute = function (instructions) {
    this.actors.forEach(actor => {
        console.log(actor)
    })
}

export default UpdateScreenCommand