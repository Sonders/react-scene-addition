import Scene from '../core/Scene'

let ReactScene = function() {
    Scene.call(this)

    this.screens = []
   // this.director.updateScene()
}

ReactScene.prototype = Object.create(Scene.prototype)

ReactScene.prototype.addScreen = function (name, screen) {
    this.screens.push({ name: name, screen: screen })
}

ReactScene.prototype.update = function () {
    this.screens.forEach(screenObject => {
        screenObject.screen.forceUpdate()
    })
}

ReactScene.instance = new ReactScene()

ReactScene.getInstance = function() {
    return ReactScene.instance
}

export default ReactScene